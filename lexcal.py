import ast

tokens = (
    'IF', 'ELSE', 'WHILE',
    'COLON', 'SEMICOLON', 'LPAR', 'RPAR', 'NEWLINE', 'COMMA',
    'LBRACKET', 'RBRACKET', 'LCBRACKET', 'RCBRACKET',
    'NAME', 'NUMBER', 'STRING',
    'PRINT', 'INPUT', 'STR', 'LEN', 'CHOP',
    'EQUAL', 'NOTEQUAL', 'GT', 'LT', 'GTE', 'LTE',
    'PLUS', 'MINUS', 'MULT', 'DIVIDE', 'POWER'
)

t_COLON = r':'
t_SEMICOLON = r'\;'
t_LPAR = r'\('
t_RPAR = r'\)'
t_COMMA = r','
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_LCBRACKET = r'{'
t_RCBRACKET = r'}'
t_PRINT = r'print'
t_INPUT = r'input'
t_STR = r'str'
t_LEN = r'len'
t_CHOP = r'chop'
t_EQUAL = r'='
t_NOTEQUAL = r'<>'
t_GT = r'>'
t_LT = r'<'
t_GTE = r'>='
t_LTE = r'<='
t_PLUS = r'\+'
t_MINUS = r'-'
t_MULT = r'\*'
t_DIVIDE = r'/'
t_POWER = r'\^'
t_ignore = ' \t'

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)
    t.type = "NEWLINE"

def t_NUMBER(t):
    r'(\d)+(\.\d+)*'
    t.value = ast.literal_eval(t.value)
    return t

def t_STRING(t):
    r"""('([^\\']+|\\'|\\\\)*')|("([^\\"]+|\\"|\\\\)*")"""
    return t

def t_comment(t):
    r"(\/\*)(\s|.)*?(\*\/)"
    pass

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

RESERVED = {
    "while": "WHILE",
    "if": "IF",
    "else": "ELSE",
    "print": "PRINT",
    "input": "INPUT",
    "str": "STR",
    "len": "LEN",
    "chop": "CHOP",
}


def t_NAME(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = RESERVED.get(t.value, "NAME")
    return t

import ply.lex as lex;lex.lex(debug=0)