'''
MAIN : LCBRACKET BLOCK RCBRACKET
BLOCK : ASSIGNMENT BLOCK
BLOCK : IFB BLOCK
BLOCK : WHILEB BLOCK
BLOCK : FUNC SEMICOLON BLOCK
BLOCK :
IFB : IF LPAR CONDITION RPAR LCBRACKET BLOCK RCBRACKET ELSE LCBRACKET BLOCK RCBRACKET
WHILEB : WHILE LPAR CONDITION RPAR LCBRACKET BLOCK RCBRACKET
WHILEB : WHILE LPAR CONDITION RPAR LCBRACKET BLOCK RCBRACKET ELSE LCBRACKET BLOCK RCBRACKET
CONDITION : EXPRESSION EQUAL EXPRESSION
CONDITION : EXPRESSION NOTEQUAL EXPRESSION
CONDITION : EXPRESSION GT EXPRESSION
CONDITION : EXPRESSION LT EXPRESSION
CONDITION : EXPRESSION GTE EXPRESSION
CONDITION : EXPRESSION LTE EXPRESSION
ASSIGNMENT : NAME COLON EQUAL EXPRESSION SEMICOLON
EXPRESSION : EXPRESSION PLUS T
EXPRESSION : EXPRESSION MINUS T
EXPRESSION : T
T : T MULT F
T : T DIVIDE F
T : F
F : NUMBER
F : STRING
F : NAME
F : FUNC
F : SUBSTR
F : LPAR EXPRESSION RPAR
F : EXPRESSION POWER EXPRESSION
SUBSTR : NAME LBRACKET NUMBER RBRACKET
SUBSTR : NAME LBRACKET NUMBER COLON RBRACKET
SUBSTR : NAME LBRACKET NUMBER COLON NUMBER RBRACKET
FUNC : INPUTF
FUNC : PRINTF
FUNC : STRF
FUNC : LENF
FUNC : CHOPF
INPUTF : INPUT LPAR FUNCSTATEMENT : EXPRESSION RPAR
FUNCSTATEMENT : EXPRESSION
FUNCSTATEMENT : 
STRF : STR LPAR EXPRESSION RPAR
LENF : LEN LPAR EXPRESSION RPAR
CHOPF : CHOP LPAR EXPRESSION RPAR
PRINTF : PRINT LPAR STATEMENT RPAR
STATEMENT : EXPRESSION COMMA STATEMENT
STATEMENT : EXPRESSION STATEMENT
STATEMENT :
'''

from assembly import Assembly
import lexcal
tokens = lexcal.tokens
asmObj = Assembly()
names = {}
resultFileName = "asm_test.txt"

precedence = (
    ('left', 'PLUS', 'MINUS'),
    ('left', 'MULT', 'DIVIDE'),
    ('left', 'POWER'),
)

def p_MAIN(p) :
    'MAIN : LCBRACKET BLOCK RCBRACKET'
    writeFile(p[2]['code'])
    p[0] = True

def p_BLOCK_ASSIGN(p) :
    'BLOCK : ASSIGNMENT BLOCK'
    p[0] = {}
    code = joinCodes( [p[1], p[2]] )
    p[0]['code'] = code

def p_BLOCK_IF(p) :
    'BLOCK : IFB BLOCK'
    p[0] = {}
    code = joinCodes( [p[1], p[2]] )
    p[0]['code'] = code

def p_BLOCK_WHILE(p) :
    'BLOCK : WHILEB BLOCK'
    p[0] = {}
    code = joinCodes( [p[1], p[2]] )
    p[0]['code'] = code

def p_BLOCK_FUNC(p) :
    'BLOCK : FUNC SEMICOLON BLOCK'
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code

def p_BLOCK_LANDA(p) :
    'BLOCK : '

def p_IF(p) :
    'IFB : IF LPAR CONDITION RPAR LCBRACKET BLOCK RCBRACKET ELSE LCBRACKET BLOCK RCBRACKET'
    lbl1 = asmObj.getLabel()
    lbl2 = asmObj.getLabel()
    if p[3]['operator'] == r'=' :
        jumpCode = asmObj.jne(lbl1)
    elif p[3]['operator'] == r'<>' :
        jumpCode = asmObj.je(lbl1)
    elif p[3]['operator'] == r'>' :
        jumpCode = asmObj.jlte(lbl1)
    elif p[3]['operator'] == r'<' :
        jumpCode = asmObj.jgte(lbl1)
    elif p[3]['operator'] == r'>=' :
        jumpCode = asmObj.jlt(lbl1)
    elif p[3]['operator'] == r'<=' :
        jumpCode = asmObj.jgt(lbl1)
    code = p[3]['code'] + jumpCode
    code += p[6]['code'] + asmObj.jmp(lbl2) + lbl1 + ": " + p[10]['code'] + lbl2 + ": "
    p[0] = {}
    p[0]['code'] = code

def p_WHILE(p) :
    'WHILEB : WHILE LPAR CONDITION RPAR LCBRACKET BLOCK RCBRACKET'
    lbl1 = asmObj.getLabel()
    lbl2 = asmObj.getLabel()
    if p[3]['operator'] == r'=' :
        jumpCode = asmObj.jne(lbl2)
    elif p[3]['operator'] == r'<>' :
        jumpCode = asmObj.je(lbl2)
    elif p[3]['operator'] == r'>' :
        jumpCode = asmObj.jlte(lbl2)
    elif p[3]['operator'] == r'<' :
        jumpCode = asmObj.jgte(lbl2)
    elif p[3]['operator'] == r'>=' :
        jumpCode = asmObj.jlt(lbl2)
    elif p[3]['operator'] == r'<=' :
        jumpCode = asmObj.jgt(lbl2)
    code = lbl1 + ": " + p[3]['code'] + jumpCode
    code += p[6]['code'] + asmObj.jmp(lbl1) + lbl2 + ": "
    p[0] = {}
    p[0]['code'] = code

def p_WHILE_ELSE(p) :
    'WHILEB : WHILE LPAR CONDITION RPAR LCBRACKET BLOCK RCBRACKET ELSE LCBRACKET BLOCK RCBRACKET'
    lbl1 = asmObj.getLabel()
    lbl2 = asmObj.getLabel()
    lbl3 = asmObj.getLabel()
    if p[3]['operator'] == r'=' :
        jumpCodeElse = asmObj.jne(lbl2)
        jumpCodeOut = asmObj.jne(lbl3)
    elif p[3]['operator'] == r'<>' :
        jumpCodeElse = asmObj.je(lbl2)
        jumpCodeOut = asmObj.je(lbl3)
    elif p[3]['operator'] == r'>' :
        jumpCodeElse = asmObj.jlte(lbl2)
        jumpCodeOut = asmObj.jlte(lbl3)
    elif p[3]['operator'] == r'<' :
        jumpCodeElse = asmObj.jgte(lbl2)
        jumpCodeOut = asmObj.jgte(lbl3)
    elif p[3]['operator'] == r'>=' :
        jumpCodeElse = asmObj.jlt(lbl2)
        jumpCodeOut = asmObj.jlt(lbl3)
    elif p[3]['operator'] == r'<=' :
        jumpCodeElse = asmObj.jgt(lbl2)
        jumpCodeOut = asmObj.jgt(lbl3)

    code = p[3]['code'] + jumpCodeElse + lbl1 + ": " + p[3]['code'] + jumpCodeOut
    code += p[6]['code'] + asmObj.jmp(lbl1) + lbl2 + ": " + p[10]['code'] + lbl3 + ": "
    p[0] = {}
    p[0]['code'] = code

def p_CONDITION(p) :
    '''CONDITION : EXPRESSION EQUAL EXPRESSION
                 | EXPRESSION NOTEQUAL EXPRESSION
                 | EXPRESSION GT EXPRESSION
                 | EXPRESSION LT EXPRESSION
                 | EXPRESSION GTE EXPRESSION
                 | EXPRESSION LTE EXPRESSION'''
    retCode = asmObj.cmp(p[1]['address'], p[3]['address'])
    asmObj.registers[p[1]['address']] = None
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code + retCode
    p[0]['operator'] = p[2]

def p_ASSIGNMENT(p) :
    'ASSIGNMENT : NAME COLON EQUAL EXPRESSION SEMICOLON'
    if p[1] not in names :
        names[p[1]] = {}
        names[p[1]]['address'] = p[4]['address']
        p[0] = {}
        p[0]['code'] = p[4]['code']
    
    else :
        code = asmObj.movToOther(names[p[1]]['address'] , p[4]['address'])
        p[0] = {}
        p[0]['code'] = p[4]['code'] + code

def p_E_E_p_T(p) :
    'EXPRESSION : EXPRESSION PLUS T'
    retCode = asmObj.add(p[1]['address'], p[3]['address'])
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code + retCode
    p[0]['address'] = p[1]['address']

def p_E_E_m_T(p) :
    'EXPRESSION : EXPRESSION MINUS T'
    retCode = asmObj.sub(p[1]['address'], p[3]['address'])
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code + retCode
    p[0]['address'] = p[1]['address']

def p_E_T(p):
    'EXPRESSION : T'
    p[0] = p[1]

def p_T_T_mul_F(p):
    'T : T MULT F'
    retCode = asmObj.mul(p[1]['address'], p[3]['address'])
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code + retCode
    p[0]['address'] = p[1]['address']

def p_T_T_div_F(p):
    'T : T DIVIDE F'
    retCode = asmObj.div(p[1]['address'], p[3]['address'])
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code + retCode
    p[0]['address'] = p[1]['address']

def p_T_F(p):
    'T : F'
    p[0] = p[1]

def p_F_NUM(p):
    'F : NUMBER'
    address,code = asmObj.mov(p[1])
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = code

def p_F_STR(p):
    'F : STRING'
    address,code = asmObj.mov(p[1])
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = code

def p_F_NAME(p):
    'F : NAME'
    if p[1] in names :
        address,code = asmObj.movAddr(names[p[1]]['address'])
        p[0] = {}
        p[0]['address'] = address
        p[0]['code'] = code
    else :
        raise BaseException("Unknown variable")

def p_F_SUBSTR(p):
    'F : SUBSTR'
    p[0] = p[1]

def p_F_lpar_E_rpar(p) :
    'F : LPAR EXPRESSION RPAR'
    p[0] = p[2]

"""def p_EXP_POWER_EXP(p):
    'F : EXPRESSION POWER EXPRESSION'
    retCode = asmObj.power(p[1]['address'], p[3]['address'])
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    code = joinCodes( [p[1], p[3]] )
    p[0]['code'] = code + retCode
    p[0]['address'] = p[1]['address']"""

def p_SUBSTR_EXPRESSION(p) :
    'SUBSTR : NAME LBRACKET EXPRESSION RBRACKET'
    if p[1] in names :
        sub = "[R%s]" % ( p[3]['address'] )
        address,code = asmObj.substr( names[p[1]]['address'], sub )
        asmObj.registers[p[3]['address']] = None
        p[0] = {}
        p[0]['address'] = address
        p[0]['code'] = p[3]['code'] + code
    else :
        raise BaseException("Unknown variable")

def p_SUBSTR_EXPRESSION_COLON(p) :
    'SUBSTR : NAME LBRACKET EXPRESSION COLON RBRACKET'
    if p[1] in names :
        sub = "[R%s:]" % ( p[3]['address'] )
        address,code = asmObj.substr( names[p[1]]['address'], sub )
        asmObj.registers[p[3]['address']] = None
        p[0] = {}
        p[0]['address'] = address
        p[0]['code'] = p[3]['code'] + code
    else :
        raise BaseException("Unknown variable")

def p_SUBSTR_COLON_EXPRESSION(p) :
    'SUBSTR : NAME LBRACKET COLON EXPRESSION RBRACKET'
    if p[1] in names :
        sub = "[:R%s]" % ( p[4]['address'] )
        address,code = asmObj.substr( names[p[1]]['address'], sub )
        asmObj.registers[p[4]['address']] = None
        p[0] = {}
        p[0]['address'] = address
        p[0]['code'] = p[4]['code'] + code
    else :
        raise BaseException("Unknown variable")

def p_SUBSTR_EXPRESSION_COLON_EXPRESSION(p) :
    'SUBSTR : NAME LBRACKET EXPRESSION COLON EXPRESSION RBRACKET'
    if p[1] in names :
        sub = "[R%s:R%s]" % ( p[3]['address'], p[5]['address'] )
        address,code = asmObj.substr( names[p[1]]['address'], sub )
        asmObj.registers[p[3]['address']] = None
        asmObj.registers[p[5]['address']] = None
        p[0] = {}
        p[0]['address'] = address
        p[0]['code'] = p[3]['code'] + p[5]['code'] + code
    else :
        raise BaseException("Unknown variable")

def p_F_FUNC(p):
    'F : FUNC'
    p[0] = p[1]

def p_FUNC_INPUT(p):
    'FUNC : INPUTF'
    p[0] = p[1]

def p_FUNC_PRINT(p):
    'FUNC : PRINTF'
    p[0] = p[1]

def p_FUNC_STR(p):
    'FUNC : STRF'
    p[0] = p[1]

def p_FUNC_LEN(p):
    'FUNC : LENF'
    p[0] = p[1]

def p_FUNC_CHOP(p):
    'FUNC : CHOPF'
    p[0] = p[1]

def p_INPUT(p):
    'INPUTF : INPUT LPAR FUNCSTATEMENT RPAR'
    address,code = asmObj.inputf( p[3]['address'] )
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = p[3]['code'] + code

def p_FUNCSTATEMENT(p):
    'FUNCSTATEMENT : EXPRESSION'
    p[0] = p[1]

def p_FUNCSTATEMENT_landa(p):
    'FUNCSTATEMENT : '
    address,code = asmObj.mov( r"''" )
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = code

def p_STR(p):
    'STRF : STR LPAR EXPRESSION RPAR'
    address,code = asmObj.strf( p[3]['address'] )
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = p[3]['code'] + code

def p_LEN(p):
    'LENF : LEN LPAR EXPRESSION RPAR'
    address,code = asmObj.lenf( p[3]['address'] )
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = p[3]['code'] + code

def p_CHOP(p):
    'CHOPF : CHOP LPAR EXPRESSION RPAR'
    address,code = asmObj.chopf( p[3]['address'] )
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = p[3]['code'] + code

def p_PRINT(p):
    'PRINTF : PRINT LPAR STATEMENT RPAR'
    code = asmObj.printf(p[3]['address'])
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    p[0]['address'] = p[3]['address']
    p[0]['code'] = p[3]['code'] + code

def p_STATEMENT_EXPRESSION_COMA(p):
    'STATEMENT : EXPRESSION COMMA STATEMENT'
    address,code = asmObj.mov( r"' '" )
    addCode = asmObj.addpr( p[1]['address'], address )
    asmObj.registers[address] = None
    addCode2 = asmObj.addpr( p[1]['address'], p[3]['address'] )
    asmObj.registers[p[3]['address']] = None
    p[0] = {}
    p[0]['address'] = p[1]['address']
    p[0]['code'] = p[1]['code'] + p[3]['code'] + code + addCode + addCode2

def p_STATEMENT_EXPRESSION(p):
    'STATEMENT : EXPRESSION STATEMENT'
    addCode = asmObj.addpr( p[1]['address'], p[2]['address'] )
    asmObj.registers[p[2]['address']] = None
    p[0] = {}
    p[0]['address'] = p[1]['address']
    p[0]['code'] = p[1]['code'] + p[2]['code'] + addCode

def p_STATEMENT_EXPRESSION_LANDA(p):
    'STATEMENT : '
    address,code = asmObj.mov( r"''" )
    p[0] = {}
    p[0]['address'] = address
    p[0]['code'] = code

def joinCodes(codes):
    returnString = ""
    for code in codes :
        if code is not None :
            returnString += code['code']
    
    return returnString

def readFile(fileName) :
    import os.path
    fileExist = os.path.isfile(fileName)
    if fileExist :
        return open(fileName,'r').read()
    else :
        raise BaseException("File not found")

def writeFile(string) :
    with open(resultFileName, mode='w') as file:
        file.write( string )

import ply.yacc as yacc; yacc.yacc()
import sys

if not (len(sys.argv) > 1) :
    raise BaseException("Please enter file name")

if len(sys.argv) > 2 :
    resultFileName = sys.argv[2]

programCode = readFile(sys.argv[1])
resultGenerated = yacc.parse(programCode)

from asmRunner import assemblyRunner
if resultGenerated :
    asmRunnerObj = assemblyRunner(resultFileName)
    asmRunnerObj.main()