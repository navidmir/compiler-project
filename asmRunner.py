import re
import ast

class assemblyRunner :
    def __init__(self,fileName = "asm_test.txt") :
        self.assembly = []
        self.fileName = fileName
        self.registers = [None for i in range(1000)]
        self.currentLine = 0
        self.sreg = 0
        self.labels = {}

        
    def readFile(self) :
        self.assembly = open(self.fileName,'r').read().splitlines()
        self.find_and_replace_first_line_labels()
        self.find_and_replace_labels()


    def find_and_replace_first_line_labels(self):
        label_pattern = re.compile("^Label[0-9]+: $")
        if label_pattern.match(self.assembly[-1]) :
            self.assembly[-1] = self.assembly[-1] + "PASS"
        label_pattern = re.compile("^Label[0-9]+")   
        for i in range(0,len(self.assembly)) :
            command = self.assembly[i]
            if label_pattern.search(command):
                label = label_pattern.search(command).group(0)
                self.labels[label] = i
                self.assembly[i] = self.assembly[i].replace(label+": ","")
                

    def find_and_replace_labels(self):
        for i in range(0,len(self.assembly)) :
            command = self.assembly[i]
            label_index = self.assembly[i].find("Label")
            if label_index != -1 :
                label = command[label_index:]
                self.assembly[i] = self.assembly[i].replace(label,str(self.labels[label]))


    def main(self) :
        self.readFile()
        while self.assembly[self.currentLine] != "PASS" :
            command = self.assembly[self.currentLine]
            operatorLength = command.find(" ")
            operator = command[:operatorLength]
            arguments = command[operatorLength+1:]
            isJumping = False

            if operator == "MOV" :
                self.mov(arguments)
            
            elif operator == "ADD" :
                self.add(arguments)
            
            elif operator == "ADDPRINT" :
                self.addpr(arguments)
            
            elif operator == "SUB" :
                self.sub(arguments)

            elif operator == "DIV" :
                self.div(arguments)

            elif operator == "MUL" :
                self.mul(arguments)

            elif operator == "INPUT" :
                self.inputf(arguments)
            
            elif operator == "PRINT" :
                self.printf(arguments)
            
            elif operator == "CHOP" :
                self.chopf(arguments)

            elif operator == "LEN" :
                self.lenf(arguments)

            elif operator == "STR" :
                self.strf(arguments)
            
            elif operator == "CMP" :
                self.cmp(arguments)

            elif operator == "SUBSTR" :
                self.substr(arguments)

            elif operator == "JMP" :
                isJumping = self.jmp(arguments)
            
            elif operator == "JE" :
                isJumping = self.je(arguments)

            elif operator == "JNE" :
                isJumping = self.jne(arguments)

            elif operator == "JGT" :
                isJumping = self.jgt(arguments)

            elif operator == "JGTE" :
                isJumping = self.jgte(arguments)
            
            elif operator == "JLT" :
                isJumping = self.jlt(arguments)
            
            elif operator == "JLTE" :
                isJumping = self.jlte(arguments)
            if not isJumping :
                self.currentLine += 1
            else:
                self.currentLine = int(arguments)

            if self.currentLine> len(self.assembly) - 1:
                return 0

    def mov(self,arguments) :
        vaIndex = arguments.index(",")
        arg1 = int(arguments[:vaIndex][1:])
        arg2 = arguments[vaIndex+1:]
        if arg2[0] == "R" :
            arg2 = int(arg2[1:])
            self.registers[arg1] = self.registers[arg2]
        
        else :
            number_pattern = re.compile("(\d)+(\.\d+)*")
            if number_pattern.match(arg2) :
                self.registers[arg1] = ast.literal_eval(arg2)
            
            else :
                self.registers[arg1] = str(arg2[1:-1])


    def add(self,arguments) :
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg1]) != type(self.registers[arg2]) and\
            (type(self.registers[arg1]) is str or type(self.registers[arg2]) is str) :
            raise BaseException("string can't add with number line:%s" % (str(self.currentLine+1)))
        self.registers[arg1] += self.registers[arg2]

    def addpr(self,arguments) :
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        self.registers[arg1] = str(self.registers[arg1]) + str(self.registers[arg2])

    def sub(self,arguments) :
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg1]) is str or type(self.registers[arg2]) is str : #age type yeki str bood error bede
            raise BaseException("string can't sub with number line:%s" % (str(self.currentLine+1)))
        self.registers[arg1] -= self.registers[arg2]


    def div(self,arguments) :
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg1]) is str or type(self.registers[arg2]) is str : #age type yeki str bood error bede
            raise BaseException("string can't divied with number line:%s" % (str(self.currentLine+1)))
        self.registers[arg1] /= self.registers[arg2]


    def mul(self,arguments) :
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg1]) is str or type(self.registers[arg2]) is str : #age type yeki str bood error bede
            raise BaseException("string can't multiple with number line:%s" % (str(self.currentLine+1)))
        self.registers[arg1] *= self.registers[arg2]


    def inputf(self,arguments) : # ERROR
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        data=input(self.registers[arg2])
        number_pattern = re.compile("(\d)+(\.\d+)*")
        if number_pattern.match(data) :
            data = ast.literal_eval(data)
        self.registers[arg1] = data

    def printf(self,arguments) : # ERROR
        arg1 = int(arguments[1:])
        print(self.registers[arg1])

    def chopf(self,arguments) : 
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg2]) is str : #age type str bood error bede
            raise BaseException("string can't CHOP line:%s" % (str(self.currentLine+1)))
        self.registers[arg1] = int(self.registers[arg2])


    def lenf(self,arguments) : 
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg2]) is not str : 
            raise BaseException("number have not len line:%s" % (str(self.currentLine+1)))
        self.registers[arg1] = len(self.registers[arg2])


    def strf(self,arguments) : 
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        self.registers[arg1] = str(self.registers[arg2])


    def cmp(self,arguments):
        arguments = arguments.split(",")
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        if type(self.registers[arg1]) is str or type(self.registers[arg2]) is str : #age type yeki str bood error bede
            raise BaseException("string can't multiple with number line:%s" % (str(self.currentLine+1)))
        result = self.registers[arg1] - self.registers[arg2]
        self.sreg = result
        
    def substr(self,arguments):
        arguments = arguments.split(',')
        arg1 = int(arguments[0][1:])
        arg2 = int(arguments[1][1:])
        arg3 = arguments[2]
        if type(self.registers[arg2]) is not str : #age type yeki str bood error bede
            raise BaseException("can't substring a number line:%s" % (str(self.currentLine+1)))
        
        pattern1 = re.compile("\[(R\d+):(R\d+)\]")
        pattern2 = re.compile("\[(R\d+):\]")
        pattern3 = re.compile("\[:(R\d+)\]")
        pattern4 = re.compile("\[(R\d+)\]")
        pattern5 = re.compile("\d+")
        if pattern1.match(arg3) :
            search = pattern5.findall(arg3)
            if type(self.registers[int(search[0])]) is not int or type(self.registers[int(search[1])]) is not int :
                raise BaseException("substring argument must be int line:%s" % (str(self.currentLine+1)))
            
            self.registers[arg1] = self.registers[arg2][self.registers[int(search[0])]:self.registers[int(search[1])]]
        
        if pattern2.match(arg3) :
            search = pattern5.findall(arg3)
            if type(self.registers[int(search[0])]) is not int :
                raise BaseException("substring argument must be int line:%s" % (str(self.currentLine+1)))
            
            self.registers[arg1] = self.registers[arg2][self.registers[int(search[0])]:]
        
        if pattern3.match(arg3) :
            search = pattern5.findall(arg3)
            if type(self.registers[int(search[0])]) is not int :
                raise BaseException("substring argument must be int line:%s" % (str(self.currentLine+1)))
            
            self.registers[arg1] = self.registers[arg2][:self.registers[int(search[0])]]

        if pattern4.match(arg3) :
            search = pattern5.findall(arg3)
            if type(self.registers[int(search[0])]) is not int :
                raise BaseException("substring argument must be int line:%s" % (str(self.currentLine+1)))
            
            if self.registers[int(search[0])] >= len(self.registers[arg2]) :
                raise BaseException("string index out of range line:%s" % (str(self.currentLine+1)))

            self.registers[arg1] = self.registers[arg2][self.registers[int(search[0])]]

    def jmp(self,arguments) : 
        return True


    def je(self,arguments) : 
        if self.sreg ==0:
            return True
        else:
            return False


    def jne(self,arguments) : 
        if self.sreg !=0:
            return True
        else:
            return False


    def jgt(self,arguments) : 
        if self.sreg > 0:
            return True
        else:
            return False
    

    def jgte(self,arguments) : 
        if self.sreg >= 0:
            return True
        else:
            return False
    

    def jlt(self,arguments) : 
        if self.sreg < 0:
            return True
        else:
            return False
    

    def jlte(self,arguments) : 
        if self.sreg <= 0:
            return True
        else:
            return False
